-- What was the max user tier for each customer?

with ondemand as 
(
select email, max(user_limit) as max_unit
from public.sale 
where financial_year = 'FY2016'
and sold_via_partner = true
and platform = 'Cloud'
and hosted_monthly = false
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
  group by 1                      
 )
 select a.max_unit, count(distinct a.email)
 from ondemand as a
group by 1
order by 1