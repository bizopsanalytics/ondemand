
 --ondemand ownership of cloud 
 with 
 ondemand_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     b.base_product not in 
                        (
                         'HipChat',
                         'Bitbucket',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon'
                        )     
        ),
 all_cloud_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'    
        ),
         partner as 
        (
                select sen 
                from public.sale 
                where sold_via_partner = false
        ),
        land_product as
        ( --coownership today
                select  contact_id, 
                        count(distinct base_product) as land_count
                from    all_cloud_paid
                where   contact_id in (select contact_id from ondemand_paid)
                group by 1
        )
                       select  case 
                        -- 1 product only
                        when a.land_count = 1 and b.base_product in ('HipChat')         then 1
                        when a.land_count = 1 and b.base_product in ('Confluence')      then 2
                        when a.land_count = 1 and b.base_product in ('JIRA')            then 3
                        when a.land_count = 1 and b.base_product in ('JIRA Software')   then 4
                        when a.land_count = 1 and b.base_product in ('JIRA Service Desk') then 5
                        when a.land_count = 1 and b.base_product in ('JIRA Core')       then 6
                        when a.land_count = 1 and b.base_product in ('Bitbucket')       then 7
                        -- Hipchat based 2 combos
                        when a.land_count = 2 and b.base_product in ('HipChat','Confluence')    then 8 
                        when a.land_count = 2 and b.base_product in ('HipChat','Bitbucket')     then 9
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA')          then 10
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Software') then 11
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Core')     then 12
                        when a.land_count = 2 and b.base_product in ('HipChat','JIRA Service Desk') then 13
                        --addon with product
                        when a.land_count = 2 and b.base_product in ('HipChat','Marketplace Addon')     then 14
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Marketplace Addon') then 15
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Marketplace Addon')   then 16
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Marketplace Addon') then 17
                        when a.land_count = 2 and b.base_product in ('JIRA','Marketplace Addon')        then 18
                        when a.land_count = 2 and b.base_product in ('Confluence','Marketplace Addon')  then 19
                        when a.land_count = 2 and b.base_product in ('Bitbucket','Marketplace Addon')   then 20
                        --JIRA family and Confluence only
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Confluence')      then 21
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Confluence')          then 22
                        when a.land_count = 2 and b.base_product in ('JIRA','Confluence')               then 23
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Confluence')  then 24
                        -- 2 own + atlassian addons
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Portfolio for JIRA')      then 25
                        when a.land_count = 2 and b.base_product in ('JIRA Software','Capture for JIRA')        then 26
                        when a.land_count = 2 and b.base_product in ('JIRA Software','JIRA Studio')             then 27
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Portfolio for JIRA')          then 28
                        when a.land_count = 2 and b.base_product in ('JIRA Core','Capture for JIRA')            then 29
                        when a.land_count = 2 and b.base_product in ('JIRA Core','JIRA Studio')                 then 30
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Portfolio for JIRA')  then 31
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','Capture for JIRA')    then 32
                        when a.land_count = 2 and b.base_product in ('JIRA Service Desk','JIRA Studio')         then 33                       
                        when a.land_count = 2 and b.base_product in ('Confluence','Team Calendars for Confluence') then 34 
                        when a.land_count = 2 and b.base_product in ('Confluence','Questions for Confluence')   then 35 
                        -- Hipchat based 3 product combos
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','Confluence')        then 36
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA')              then 37
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Software')     then 38 
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Core')         then 39
                        when a.land_count = 3 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk') then 40
                        -- addons with 3
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Confluence','Marketplace Addon')  then 41  
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Confluence','Marketplace Addon')      then 42
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Confluence','Marketplace Addon') then 43
                        when a.land_count = 3 and b.base_product in ('JIRA','Confluence','Marketplace Addon')           then 44
                        when a.land_count = 3 and b.base_product in ('Bitbucket','Confluence','Marketplace Addon')      then 45
                        when a.land_count = 3 and b.base_product in ('HipChat','Confluence','Marketplace Addon')        then 46
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Bitbucket','Marketplace Addon')   then 47
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Bitbucket','Marketplace Addon')       then 48
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Bitbucket','Marketplace Addon') then 49
                        when a.land_count = 3 and b.base_product in ('JIRA','Bitbucket','Marketplace Addon')            then 50
                        when a.land_count = 3 and b.base_product in ('JIRA Software','HipChat','Marketplace Addon')     then 51
                        when a.land_count = 3 and b.base_product in ('JIRA Core','HipChat','Marketplace Addon')         then 52
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','HipChat','Marketplace Addon') then 53
                        when a.land_count = 3 and b.base_product in ('JIRA','HipChat','Marketplace Addon')              then 54
                        --Bitbucket based 3 product combos
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA','Confluence')           then 55
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Software','Confluence')  then 56
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Core','Confluence')      then 57
                        when a.land_count = 3 and b.base_product in ('Bitbucket','JIRA Service Desk','Confluence') then 58
                         -- 3 own + atlassian addons
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Portfolio for JIRA','Capture for JIRA')   then 59
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Portfolio for JIRA','JIRA Studio')        then 60
                        when a.land_count = 3 and b.base_product in ('JIRA Software','Capture for JIRA','JIRA Studio')          then 61
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Portfolio for JIRA','Capture for JIRA')       then 62
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Portfolio for JIRA','JIRA Studio')            then 63
                        when a.land_count = 3 and b.base_product in ('JIRA Core','Capture for JIRA','JIRA Studio')              then 64
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Portfolio for JIRA','Capture for JIRA') then 65
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Portfolio for JIRA','JIRA Studio')    then 66
                        when a.land_count = 3 and b.base_product in ('JIRA Service Desk','Capture for JIRA','JIRA Studio')      then 67                                             
                        when a.land_count = 3 and b.base_product in ('Confluence','Team Calendars for Confluence','Questions for Confluence') then 68                      
                        -- 4 product combos
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA','Confluence') then 69
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software','Confluence') then 70
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core','Confluence') then 71
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk','Confluence') then 72
                        -- 4 with addon
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','Confluence', 'Marketplace Addon') then 73
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Software', 'Marketplace Addon') then 74
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Core', 'Marketplace Addon') then 75
                        when a.land_count = 4 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'Marketplace Addon') then 76
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA', 'Marketplace Addon') then 77
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Software', 'Marketplace Addon') then 78
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Core', 'Marketplace Addon') then 79
                        when a.land_count = 4 and b.base_product in ('Confluence','Bitbucket','JIRA Service Desk', 'Marketplace Addon') then 80  
                        -- 4 own + atlassian addons
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Core','Portfolio for JIRA','Capture for JIRA')   then 81                        
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Core','Portfolio for JIRA','JIRA Studio')        then 82
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Core','Capture for JIRA','JIRA Studio')          then 83   
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Service Desk','Portfolio for JIRA','Capture for JIRA')   then 84                        
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Service Desk','Portfolio for JIRA','JIRA Studio')        then 85
                        when a.land_count = 4 and b.base_product in ('JIRA Software','JIRA Service Desk','Capture for JIRA','JIRA Studio')          then 86                      
                        when a.land_count = 4 and b.base_product in ('JIRA Core','JIRA Service Desk','Portfolio for JIRA','Capture for JIRA')       then 87
                        when a.land_count = 4 and b.base_product in ('JIRA Core','JIRA Service Desk','Portfolio for JIRA','JIRA Studio')            then 88
                        when a.land_count = 4 and b.base_product in ('JIRA Core','JIRA Service Desk','Capture for JIRA','JIRA Studio')              then 89                                           
                        when a.land_count = 4 and b.base_product in ('JIRA Software','Confluence','Team Calendars for Confluence','Questions for Confluence') then 90 
                        when a.land_count = 4 and b.base_product in ('JIRA Core','Confluence','Team Calendars for Confluence','Questions for Confluence') then 91   
                        when a.land_count = 4 and b.base_product in ('JIRA Service Desk','Confluence','Team Calendars for Confluence','Questions for Confluence') then 92            
                        -- > 4 product combos                 
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA','JIRA Software','Confluence') then 93
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Software','JIRA Core','Confluence') then 94
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Core','JIRA Service Desk','Confluence') then 95
                        when a.land_count = 5 and b.base_product in ('HipChat','Bitbucket','JIRA Service Desk', 'JIRA Software','Confluence') then 96
                else 97
                end as coown_group,
                count(distinct a.contact_id)
        from land_product as a
        left join  all_cloud_paid as b on a.contact_id = b.contact_id
        where b.sen in (select sen from partner)
        group by 1
        order by 1
  