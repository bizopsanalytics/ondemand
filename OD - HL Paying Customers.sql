--Paying ondemand customers

with all_paid as
 (
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.parent_sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        --and     c.level in ('Full', 'Starter')   
        --and     c.level in ('Free')
        and     c.level in ('Evaluation')
        --and     c.level in ('Full', 'Starter','Free','Evaluation')   
        and     b.base_product not in 
                        (
                         'HipChat',
                         'Bitbucket',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon'
                        )
        and     b.platform = 'Cloud'
        )
        select  --base_product, 
                --level,
                count(distinct customer_id) as customer_count, 
                count(distinct contact_id) as contact_count,                
                count(distinct parent_sen) as instance_count,
                count(distinct sen) as sen_count
        from all_paid
        --group by 1
        --order by 4 desc
        
        
        