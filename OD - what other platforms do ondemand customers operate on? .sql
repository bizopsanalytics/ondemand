-- what other platforms do ondemand customers operate on?

with ondemand as 
(
select email
from public.sale 
where financial_year = 'FY2016'
and sold_via_partner = false
and platform = 'Cloud'
and hosted_monthly = false
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 )
 
 select b.platform, count(distinct b.email)
 from ondemand as a
 left join public.sale as b on a.email = b.email
 where financial_year = 'FY2016'
 group by 1
 