with annual as
(
select  sen
from public.sale 
where   financial_year = 'FY2016'
and     sold_via_partner = true
and     platform = 'Cloud'
and     hosted_monthly = false
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        )                     
  )
  ,
   monthly as
(
select  sen
from public.sale 
where   financial_year = 'FY2016'
and     sold_via_partner = true
and     platform = 'Cloud'
and     hosted_monthly = true
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        )                     
  )
  
  select a.*
  from annual as a
  join monthly as b on a.sen = b.sen
  group by 1