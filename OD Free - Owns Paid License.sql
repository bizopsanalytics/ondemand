
 --high level free ondemand customers
 with 
 ondemand_free as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Free')  
        and     c.billing_period = 'Monthly'
        and     b.platform = 'Cloud'
        and     b.base_product not in 
                        (
                         'HipChat',
                         'Bitbucket',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon'
                        )     
        )
        ,
        all_paid as
 (--identify the Cloud products that are owned by the HipChat cloud cohort.
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')  
        --and     c.billing_period = 'Monthly'
        --and     b.platform = 'Cloud'    
        )
        select count(distinct customer_id) as domains,
               count(distinct contact_id) as contacts,
               count(distinct sen) as sens
        from ondemand_free
        where contact_id not in (select contact_id from all_paid)
 
     ;
        
 