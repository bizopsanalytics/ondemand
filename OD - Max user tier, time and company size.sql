-- how long have the ondemand customers been a paying atlassian customer?

with first_date as 
(
select  email,
        min(date) as min_date
from public.sale 
group by 1          
  ),
tier as 
(
select email, 
        max(user_limit) as max_count
from public.sale 
where financial_year = 'FY2016'
group by 1
)
  select year(b.min_date), month(b.min_date), c.max_count, d.company_size, count(distinct a.email)
  from public.sale as a
  left join first_date as b on a.email = b.email
  left join tier as c on a.email = c.email
  left join zone_bizops.customer_size as d on a.email_domain = d.email_domain
  where   financial_year in ('FY2016')
and     a.sold_via_partner = false
and     a.platform = 'Cloud'
and     a.hosted_monthly = true
and     a.base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 
  group by 1,2,3,4
  order by 1,2,3,4