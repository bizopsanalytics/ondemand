-- How many customers purchased both annual and monthly?

with ondemand as 
(
select email
from public.sale 
where financial_year = 'FY2016'
and sold_via_partner = true
and platform = 'Cloud'
and hosted_monthly = true
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 ),
 annual as 
(
select email
from public.sale 
where financial_year = 'FY2016'
and sold_via_partner = true
and platform = 'Cloud'
and hosted_monthly = false
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 )
 
 select count(distinct b.email)
 from ondemand as a
 join annual as b on a.email = b.email
 