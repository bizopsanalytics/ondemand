with od_cust as 
(
select email
from public.sale 
where sold_via_partner = true
and 
financial_year = 'FY2016'
and platform = 'Cloud'
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        )  
)                       
                    
select sum(amount)
from public.sale
where financial_year = 'FY2016'
and email in (select email from od_cust)   