--bookings for ondemand products

  select --email_domain,
                               sum(amount) as amount
                        from   public.sale
                        where  financial_year = 'FY2016'
                        and platform = 'Cloud'
                        and base_product not in 
                                (
                                 'HipChat',
                                 'Bitbucket',
                                 'Bamboo',
                                 'StatusPage',
                                 'Atlassian University',
                                 'FishEye/Crucible',
                                 'Marketplace Addon'
                                )

;
-- heatmap for company size by company size
    with bookings as 
         (
                        select email_domain,
                               sum(amount) as amount
                        from   public.sale
                        where  financial_year = 'FY2016'
                        group by 1
         ),    
       hc_paid as
 (--identify all the HipChat Cloud customers
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')   
        and     b.base_product not in 
                        (
                         'HipChat',
                         'Bitbucket',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon'
                        ) 
        and     b.platform = 'Cloud'
        )
        select  sum(amount) as amount
        from    bookings
        where email_domain in (select smart_domain from hc_paid)     
        --group by 1
        order by 1           
   
      