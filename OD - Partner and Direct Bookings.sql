--partner bookings overall

select sum(amount)
from public.sale 
where sold_via_partner = true
and financial_year = 'FY2016'
  
;        
--bookings for ondemand sold in fy16 via a partner?

select --base_product, 
        sum(amount)
from public.sale 
where --sold_via_partner = false
--and 
financial_year = 'FY2016'
and platform = 'Cloud'
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        )  
-- Group by 1                       
                    