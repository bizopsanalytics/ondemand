with bb_ondemand as
 (       
        select  
                a.customer_id,
                a.contact_id,
                a.license_id,
                b.base_product,
                b.platform,
                c.sen,
                c.type,
                c.unit_count,
                c.billing_period,
                c.level,
                c.account_type,
                d.smart_domain         
        from    model.fact_license_active as a
        join    model.dim_product as b on a.product_id = b.product_id
        join    model.dim_license as c on a.license_id = c.license_id
        join    model.dim_customer as d on a.customer_id = d.customer_id
        where   a.date_id = 20161101
        and     c.level in ('Full', 'Starter')  
        and     b.platform = 'Cloud'
        and     c.billing_period = 'Annual'
        and     b.base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon'
                        )  
)
select  count(distinct customer_id) as domains,
        count(distinct contact_id) as contacts,
        count(distinct sen) as sens
from bb_ondemand