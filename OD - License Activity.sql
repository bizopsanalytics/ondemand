with ondemand as 
(
select  month,
        sale_type,
        count(distinct invoice),
        count(distinct email),
        sum(amount)
from public.sale 
where   financial_year = 'FY2016'
and     sold_via_partner = false
and     platform = 'Cloud'
and     hosted_monthly = true
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 group by 1,2                   
  )
  
  select * from ondemand
  order by 1,2
  
  ;
  -- marketplace activity
  with ondemand as 
(
select  month,
        sale_type,
        count(distinct invoice),
        count(distinct email),
        sum(amount)
from public.sale 
where   financial_year = 'FY2016'
and     sold_via_partner = false
and     platform = 'Cloud'
and     hosted_monthly = true
and     base_product in 
                        (                        
                         'Marketplace Addon'
                        ) 
 group by 1,2                   
  )
  
  select * from ondemand
  order by 1,2
  