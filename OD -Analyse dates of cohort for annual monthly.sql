with groups as
(
select sen, 
        case when hosted_monthly = true then date else null end as "mth",
        case when hosted_monthly = false then date else null end as "ann"
        
from public.sale 
where sen in (
'6170590',
'7226187',
'5425463',
'5904979',
'5372513',
'6109623',
'3975554',
'4407409',
'4658220',
'4407408',
'3979056',
'2470503',
'5161382',
'5372512',
'3982984',
'3187286',
'6409661',
'2838649',
'4515884',
'3365496',
'5425466',
'3365499',
'3665157',
'2459806',
'6268195',
'6866548',
'3665158',
'6422873',
'3979058',
'3665155',
'5729617',
'3181078',
'3365497',
'4407407',
'5892499',
'5586002',
'3346919',
'5425465')
group by 1,2,3
)
select sen, min(mth) as min_mth, min(ann) as min_ann
from groups
group by 1

