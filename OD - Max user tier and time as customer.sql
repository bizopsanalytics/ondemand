-- how long have the ondemand customers been a paying atlassian customer?

with first_date as 
(
select  email,
        min(date) as min_date
from public.sale 
group by 1               
  ),
tier as 
(
select email, 
        max(user_limit) as max_count
from public.sale 
where financial_year = 'FY2016'
group by 1
)
  select year(b.min_date), month(b.min_date), c.max_count, count(distinct a.email)
  from public.sale as a
  left join first_date as b on a.email = b.email
  left join tier as c on a.email = c.email
  where   financial_year in ('FY2016')
and     sold_via_partner = false
and     platform = 'Cloud'
and     hosted_monthly = true
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 
  group by 1,2,3
  order by 1,2,3