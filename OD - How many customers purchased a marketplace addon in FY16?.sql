-- How many customers purchased a marketplace addon in FY16?

with ondemand as 
(
select email
from public.sale 
where financial_year = 'FY2016'
and sold_via_partner = false
and platform = 'Cloud'
and hosted_monthly = false
and     base_product not in 
                        (
                         'Bitbucket',
                         'HipChat',
                         'Bamboo',
                         'StatusPage',
                         'Atlassian University',
                         'FishEye/Crucible',
                         'Marketplace Addon',
                         'FishEye',
                         'Crowd',
                         'Atlassian Technical Account Management',
                         'Crucible',
                         'Atlassian Premier Support',
                         'Training',
                         'Clover'
                        ) 
 )
 select count(distinct a.email)
 from ondemand as a
 left join public.sale as b on a.email = b.email
 where b.base_product = 'Marketplace Addon'
 and b.platform = 'Cloud'
 and b.financial_year = 'FY2016'